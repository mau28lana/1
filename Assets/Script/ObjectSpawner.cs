using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectSpawner : MonoBehaviour
{
    public GameObject[] Cars;
    // public GameObject bus;
    public Transform[] carPosition;

    void Start()
    {
        // InvokeRepeating("SpawnObject0", 5, 20);
        InvokeRepeating("SpawnObject1", 2, 2);
        // InvokeRepeating("SpawnObject2", 2, 6);
        // InvokeRepeating("SpawnObject3", 4, 5);
    }

    //  public void SpawnObject0()
    // {
    //     // yield return new WaitForSeconds(5);
    //     Instantiate(bus, carPosition[0].position, carPosition[0].rotation);
    // }

    public void SpawnObject1()
    {
        // yield return new WaitForSeconds(3);
        int randomIndex = Random.Range(0, Cars.Length);
        Instantiate(Cars[randomIndex], carPosition[0].position, carPosition[0].rotation);
    }

    // public void SpawnObject2()
    // {
    //     // yield return new WaitForSeconds(4);
    //     int randomIndex = Random.Range(0, Cars.Length);
    //     Instantiate(Cars[randomIndex], carPosition[2].position, carPosition[2].rotation);
    // }

    // public void SpawnObject3()
    // {
    //     // yield return new WaitForSeconds(3);
    //     int randomIndex = Random.Range(0, Cars.Length);
    //     Instantiate(Cars[randomIndex], carPosition[3].position, carPosition[3].rotation);
    // }
}
